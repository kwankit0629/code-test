import { Request, Response } from "express";
import fetch from "node-fetch";
import { Service } from "../service/service";

export class Controller {
  static getAllItems: any;
  constructor(private Service: Service) {}

  getAllItems = async (req: Request, res: Response) => {
    try {
      let allItems = await this.Service.getAllItem();
      res.json(allItems);
    } catch (error) {
      console.log(error);
    }
  };
  getItemById = async (req: Request, res: Response) => {
    try {
      const category_id = parseInt(req.params.id);
      let cat = await this.Service.getItemById(category_id);

      res.json(cat);
    } catch (error) {
      console.log(error);
    }
  };
}
