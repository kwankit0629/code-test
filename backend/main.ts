import express from "express";
import { Request, Response } from "express";
import Knex from "knex";
import dotenv from "dotenv";
import { Controller } from "./controller/controller";

dotenv.config();

const knexConfigs = require("./knexfile");
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];
const knex = Knex(knexConfig);

const app = express();
app.use(express.json());

app.get("/", function (req: Request, res: Response) {
  res.end("Hello World");
});
app.get("/allitem", function (req: Request, res: Response) {
  Controller.getAllItems;
});

const PORT = 8080;

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
