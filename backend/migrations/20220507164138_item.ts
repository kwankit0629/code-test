import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("item", (table) => {
    table.increments();
    table.string("name");
    table.float("price");
    table.string("photo");
    table.integer("category_id").unsigned();
    table.foreign("category_id").references("category.id");
    table.timestamps(false, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("item");
}
