import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  //   await knex("table_name").del();

  // Inserts seed entries
  await knex("category").insert([
    { id: 1, name: "包類" },
    { id: 2, name: "意粉" },
    { id: 3, name: "小食" },
    { id: 4, name: "湯羹" },
    { id: 5, name: "飲品" },
  ]);

  await knex("item").insert([
    {
      id: 1,
      name: "芝士漢堡包",
      price: 30.0,
      photo: "芝士漢堡包.jpg",
      category_id: 1,
    },
    {
      id: 2,
      name: "雞排漢堡包",
      price: 35.0,
      photo: "雞排漢堡包.jpg",
      category_id: 1,
    },
    {
      id: 3,
      name: "豬排漢堡包",
      price: 30.0,
      photo: "豬排漢堡包.jpg",
      category_id: 1,
    },
    { id: 4, name: "漢堡包", price: 20.0, photo: "漢堡包.jpg", category_id: 1 },
  ]);
}
