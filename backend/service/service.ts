import { Knex } from "knex";

export class Service {
  constructor(private knex: Knex) {}

  async getItemById(catId: number) {
    return await this.knex.select("*").from("item").where("category_id", catId);
  }
  async getAllItem() {
    return await this.knex.select("*").from("item");
  }
}
